# Nfq akademijos namų darbai
# Pridėtas failų autoload.
# Sukurtos 4 calculateHomeWorkSum funkcijos.
# 1 funkcija susumuoją visų paduotų parametrų tipus: integer, float, string.
# 2 funkcija susumuoją parametrus paversdamus float ir string tipus į integer.
# 3 funkcija susumuoją parametrus paversdamus float ir string tipus į integer. 
# 4 funkcija naudoja declare(strict_types=1); todėl padavus parametrus: (3, 2.2, '1') gaunama klaida kadangi ne visi parametrai nurodyti integer tipo.

