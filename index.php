<?php

require 'vendor/autoload.php';

use function Nfq\Akademija\Not_Typed\calculateHomeWorkSum as calculate;
use function Nfq\Akademija\Soft\calculateHomeWorkSum as calculateSoft;
use function Nfq\Akademija\Strict\calculateHomeWorkSum as calculateStrict;
echo \calculateHomeWorkSum(3, 2.2, '1');
echo '<br>';
echo calculate(3, 2.2, '1');
echo '<br>';
echo calculateSoft(3, 2.2, '1');
echo '<br>';
echo calculateStrict(3, 2.2, '1');

